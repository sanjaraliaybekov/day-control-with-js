function getModal() {
    $(document).ready(function () {
        $('#startModal').modal('show');
    });
}


users = [];
userResult = [];
let temmmmp;

function getModalTwo(i) {
    $(document).ready(function () {
        $('#startModalTwo').modal('show');
    });
    temmmmp = i;
}

function userBuy() {
    let userMoneyHarojat = parseInt(document.forms['modalBodyForm2']['cashCount'].value);
    let userHarojatSabab = document.forms['modalBodyForm2']['attachment'].value;
    if (userMoneyHarojat > 0 && userHarojatSabab.trim().length > 0) {
        let newHarojat = {
            userMoneyHarojat: userMoneyHarojat,
            userHarojatSabab: userHarojatSabab,
            temmmmp
        };
        if (users[temmmmp].userMoney - userMoneyHarojat >= 0) {
            userResult.push(newHarojat);
            drawBuy();
            users[temmmmp].userMoney = users[temmmmp].userMoney - userMoneyHarojat;
            drawUsers();
            document.forms['modalBodyForm2'].reset();

        } else {
            alert("Mablag' yetarli emas!")
        }
    } else alert("Ma'lumotlarni kiriting!")
    tab_tab();
}


function drawBuy() {
    document.getElementById('tBody1').innerHTML = '';
    for (let i = 0; i < userResult.length; i++) {
        document.getElementById('tBody1').innerHTML += '<tr>' + (userResult[i].temmmmp + 1) + '-Foydalanuvchi' + '</tr>' + '<tr>' +
            '<td class="kirim`">' + userResult[i].userMoneyHarojat + '</td>' +
            '<td class="d-flex w-100 justify-content-between align-items-center">' + userResult[i].userHarojatSabab + '<button type="button" onclick="deleteInfo(' + i + ',1)" class="btn btn-outline-danger">' + '<img src="img/delete.svg" class="gain1">' +
            '</button>' + '</td>' +
            '</tr>';
    }
}

userResult2 = [];

function userGet() {
    let userHarojatSabab = document.forms['modalBodyForm2']['attachment'].value;
    let userMoneyMablag = parseInt(document.forms['modalBodyForm2']['cashCount'].value);
    if (userHarojatSabab.trim().length > 0 && userMoneyMablag > 0) {
        let newDaromad = {
            userHarojatSabab: userHarojatSabab,
            userMoneyMablag: userMoneyMablag,
            temmmmp
        };
        userResult2.push(newDaromad);
        drawGet();
        users[temmmmp].userMoney = users[temmmmp].userMoney + userMoneyMablag;
        drawUsers();
        document.forms['modalBodyForm2'].reset();
    } else {
        alert("Ma'lumotlarni kiriting!")
    }
    tab_tab();
}


function drawGet() {
    document.getElementById('tBody2').innerHTML = '';
    for (let i = 0; i < userResult2.length; i++) {
        document.getElementById('tBody2').innerHTML += '<tr>' + (userResult2[i].temmmmp + 1) + '-Foydalanuvchi' + '</tr>' +
            '<tr>' +
            '<td class="kirim">' + userResult2[i].userMoneyMablag + '</td>' +
            '<td class="d-flex w-100 align-items-center justify-content-between">' + userResult2[i].userHarojatSabab + '<button type="button" onclick="deleteInfo(' + i + ',0)" class="btn btn-outline-danger">' + '<img src="img/delete.svg" class="gain1">' +
            '</button>' + '</td>' +
            '</tr>';
    }

}

function deleteInfo(id, a) {
    if (a === 0) {
        userResult2.splice(id, 1);
        drawGet();
    } else {
        userResult.splice(id, 1);
        drawBuy();
    }
}


function addUser() {
    let userName = document.forms['modalBodyForm']['userName'].value;
    let userSurname = document.forms['modalBodyForm']['userSurname'].value;
    let userMoney = parseInt(document.forms['modalBodyForm']['userMoney'].value);
    if (userName.trim().length > 0 && userSurname.trim().length > 0 && userMoney > 0) {
        let newUser = {
            userName: userName,
            userSurname: userSurname,
            userMoney: userMoney
        };
        users.push(newUser);
        drawUsers();
        document.forms['modalBodyForm'].reset();
    } else {
        alert("Ma'lumotlarni kiriting!")
    }
}


function drawUsers() {
    document.getElementById('userList').innerHTML = '';
    for (let i = 0; i < users.length; i++) {
        document.getElementById('userList').innerHTML +=
            '<div class="col-md-4 col-6 mt-3">' +
            '<div class="card h-100">' + '<div class="card-header">' +
            '<h4>' + 'Ism: ' + users[i].userName + '</h4>' +
            '<h4>' + 'Familiya: ' + users[i].userSurname + '</h4>' +
            '<h4>' + 'Mablag\'ingiz: ' + users[i].userMoney + ' UZS' + '</h4>' +
            '</div>' +
            '<div class="card-body">' + '<button class="btn btn-block btn-success" onclick="getModalTwo(' + i + ')" type="button"><img src="img/pay.svg" class="w-25" alt=""></button>' +
            '</div>' +
            '</div>' +
            '</div>'

    }
}

function tab_tab() {

    var $gallery = $(".tm-gallery").isotope({
        itemSelector: ".tm-gallery-item",
        layoutMode: "fitRows"
    });
    // layout Isotope after each image loads
    $gallery.imagesLoaded().progress(function () {
        $gallery.isotope("layout");
    });

    $(".filters-button-group").on("click", "a", function () {
        var filterValue = $(this).attr("data-filter");
        $gallery.isotope({filter: filterValue});
    });

    $(".tabgroup > div").hide();
    $(".tabgroup > div:first-of-type").show();
    $(".tabs a").click(function (e) {
        e.preventDefault();
        var $this = $(this),
            tabgroup = "#" + $this.parents(".tabs").data("tabgroup"),
            others = $this
                .closest("li")
                .siblings()
                .children("a"),
            target = $this.attr("href");
        others.removeClass("active");
        $this.addClass("active");
    });
}
tab_tab();